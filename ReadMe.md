## Franc Online Shopping
Franc is an e-commerce website that aims at enabling 
people to buy different kinds of products. It is picked up 
with online payment gateway called YenePay to process 
payments online without the need for cash or direct 
bank transfers during orders.

## Tools Used
To build this website I used HTML and CSS, Javascript, 
jQuery library, PHP for backend and PHPMailer library 
for email verification.
