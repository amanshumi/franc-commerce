<?php  
	include_once 'php/db/connection.php';
	session_start();

	if (!isset($_SESSION['user_id'])) {
		header("Location: login.php");
	} 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Cart</title>
	<link rel="stylesheet" type="text/css" href="styles/index.css">
	<script src="scripts/jquery-3.4.1.js"></script>
</head>
<body>	
	<div id="all">
		<div id="app" class="head">
			<div class="nav">
				<div id="brand">
					<p>Frank</p>
				</div>
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="#" class="active">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button style="font-family: material;" class="logout" type="submit" name="logout">exit_to_app</button>
					</form>
				</div>
				<div id="collapse">
					<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
				</div>
			</div>

			<div class="mobile-nav">
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="#" class="activee">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button class="llogout" type="submit" name="logout">logout</button>
					</form>
				</div>
			</div>

			<script>
				let switch_to = true;

				$("#collapse").hover(function() {
					$(this).css({opacity: "0.7", transition: "0.2s"});
				}, function() {
					$(this).css({opacity: "1", transition: "0.2s"});
				})

				$("#collapse").click(function() {
					this.switch_to = !this.switch_to;

					if(this.switch_to){
						$("#shit").text("clear");
					} else {
						$("#shit").text("menu");
					}

					$(".mobile-nav").slideToggle("slow");
				})
			</script>
		</div>
	
		<div class="body" style="margin-top: 100px;">
			<div id="uselessd" class="title" style="font-family: Poppins;">
				<span style="font-family: material; padding-right: 10px; font-size: 45px;">shopping_cart</span>
				<p style="padding: 20px; font-size: 25px;">shopping cart</p>
			</div>

			<div id="pro-all" class="pro-all-cart" style="width: 70%;">
				<?php  
					if (isset($_GET['id'])) {
						$to_remove = htmlspecialchars(mysqli_real_escape_string($conn, $_GET['id']));
						$user_id = $_SESSION['user_id'];

						if(!filter_var($to_remove, FILTER_VALIDATE_INT)) {
							exit();
						}

						$get_quantity = "select cart.quantity, product.product_quantity from franc.cart, franc.product where cart.product_id=$to_remove and cart.user_id=$user_id and product.product_id=cart.product_id;";
						$quantity_query = mysqli_query($conn, $get_quantity);

						if(mysqli_num_rows($quantity_query) > 0) {
							if($quantity_data = mysqli_fetch_assoc($quantity_query)) {
								$quantity_in_cart = $quantity_data['quantity'] + $quantity_data['product_quantity'];

								$update_product = "update franc.product set product_quantity=$quantity_in_cart where product.product_id=$to_remove;";
								$update_product_query = mysqli_query($conn, $update_product);

								$sql_remove = "delete from franc.cart where cart.product_id=$to_remove and cart.user_id=$user_id;";
								$query_delete = mysqli_query($conn, $sql_remove);
							}
						}
					}

					$user_id = $_SESSION['user_id'];

					$stmt = "select * from franc.cart, franc.product where cart.user_id=$user_id and product.product_id=cart.product_id;";
					$result = mysqli_query($conn, $stmt);

					if(mysqli_num_rows($result) > 0) {
						while($product_row = mysqli_fetch_assoc($result)) {
							echo "<div id='cart-product'>
									<img src='images/".$product_row['product_name'].".jpeg'>";
							echo "	<div id='spec'>
								    	<div><h3>".$product_row['product_name']."</h3><p>quantity: ".$product_row['quantity']."</p><p>price: ".$product_row['product_price']."ETB</p>";
							echo "		</div>
									<div>
									<a id='mad' style='font-family: material; font-size: 20px;' href='cart.php?id=".$product_row['product_id']."'>delete</a>
								  </div>";
							echo "</div>";
							echo "</div>";
						}
					} else {
						echo "<div id='cart-product' style='margin-bottom: 200px;'>";
						echo "<div id='spec'>
							    <div><h3></h3><p>There are 0 products in your cart.</p><p></p>";
						echo "</div><div></div>";
						echo "</div>";
						echo "</div>";
					}
				?>
			</div>

			<?php
				$user_id = $_SESSION['user_id'];
				
				$stm = "select product_price, cart.quantity from franc.cart, franc.product where cart.user_id=$user_id and product.product_id=cart.product_id;";
				$resultt = mysqli_query($conn, $stm);

				$single_price = 0.0;
				$overall_price = 0.0;
				if(mysqli_num_rows($resultt) > 0) {
					while($product_row = mysqli_fetch_assoc($resultt)) {
						$single_price = $product_row['product_price'] * $product_row['quantity'];
						$overall_price += $single_price;
					}

					echo "<div id='pay-form'><p>Total Price: ".$overall_price." ETB</p>
					<a href='billing.php' id='button'>place order</a></div>";
				}
			?>

			<script>
				// $(function() {
				// 	$("#button").click(function() {
				// 		$("#uselessd").slideUp("fast");
				// 		$("#pro-all").slideUp("fast");
				// 		$("#pay-form").slideUp("fast");
				// 		$("#form-payment").slideDown("fast");
				// 	})

				// 	$("#exit-form").click(function() {
				// 		$("#uselessd").slideDown("fast");
				// 		$("#pro-all").slideDown("fast").css({"display": "grid"});
				// 		$("#pay-form").slideDown("fast").css({"display": "flex"});
				// 		$("#form-payment").slideUp("fast");
				// 	})
				// });	
			</script>
		</div>

		<?php include 'includes/footer.php'; ?>

		<script>
			$(function() {
				$("#pay-form-data").submit(function(e) {
					e.preventDefault();
				})
			})
		</script>
	</div>

	<script>
		let show = false;
		
		function toggleShow() {
			this.show = !this.show;
			console.log(this.show);

			if(this.show) {
				document.getElementById('list').style.display = 'flex';
				document.getElementById('least').innerHTML= 'expand_less';
			} else {
				document.getElementById('list').style.display = 'none';
				document.getElementById('least').innerHTML= 'expand_more';
			}
		}	

		function show_modal() {
			let doc = document.getElementById('modal');
			doc.style.display = 'flex';
		}		

		function hide_modal() {
			document.getElementById('modal').style.display = 'none';
		}	
	</script>
	<script src="js/script.js"></script>
	
</body>
</html>