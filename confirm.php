<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Confirm Account</title>
	<link rel="stylesheet" type="text/css" href="styles/login.css">
	<script src="scripts/jquery-3.4.1.js"></script>
</head>
<body>
	<div id="main">
	<div class="nav">
			<div id="brand">
				<p>frank</p>
			</div>
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="signup.php">signup</a>
			</div>
			<div id="collapse">
				<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
			</div>
		</div>
		<div class="mobile-nav">
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="signup.php">signup</a>
			</div>
		</div>

		<script>
			let switch_to = true;

			$("#collapse").hover(function() {
				$(this).css({opacity: "0.7", transition: "0.2s"});
			}, function() {
				$(this).css({opacity: "1", transition: "0.2s"});
			})

			$("#collapse").click(function() {
				this.switch_to = !this.switch_to;

				if(this.switch_to){
					$("#shit").text("clear");
				} else {
					$("#shit").text("menu");
				}

				$(".mobile-nav").slideToggle("slow");
			})
		</script>

		<div class="form-con">
			<div id="title">
				<h1>Confirm Your Account</h1>
			</div>

			<div id="form-gr" style="opacity: 0.8; margin: auto; border-left: 10px solid #B629D8; padding: 0px; font-size: 13px; width: 85%;">
				<p style="line-height: 2.0em; margin-left: 20px;">please go to your email and validate your account before logging in to our platform. to do so you just need to click on the link we have sent to your email address.</p>
			</div>
		</div>
		<div id="copyright" style="padding: 5px; width: 80%; margin: 150px auto;">
			<p style="font-family: poppins; text-align: center;">Frank &copy; 2020 All Rights Reserved.</p>
		</div>
	</div>
</body>
</html>