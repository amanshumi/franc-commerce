<?php  
	session_start();

	if (isset($_SESSION['user_id'])) {
		header("Location: home.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Find Account</title>
	<link rel="stylesheet" type="text/css" href="styles/login.css">
	<script src="scripts/jquery-3.4.1.js"></script>
</head>
<body>
	<div id="main">
		<div class="nav">
			<div id="brand">
				<p>Frank</p>
			</div>
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="signup.php">signup</a>
			</div>
			<div id="collapse">
				<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
			</div>
		</div>
		<div class="mobile-nav">
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="signup.php">signup</a>
			</div>
		</div>

		<script>
			let switch_to = true;

			$("#collapse").hover(function() {
				$(this).css({opacity: "0.7", transition: "0.2s"});
			}, function() {
				$(this).css({opacity: "1", transition: "0.2s"});
			})

			$("#collapse").click(function() {
				this.switch_to = !this.switch_to;

				if(this.switch_to){
					$("#shit").text("clear");
				} else {
					$("#shit").text("menu");
				}

				$(".mobile-nav").slideToggle("slow");
			})
		</script>

		<div class="form-con">
			<div id="title">
				<h1>Find Your Account</h1>
			</div>

			<div id="form-gr">
				<form action="php/find.php" method="POST">
					<div class="special" id="input" style="font-family: poppins;">
						<p>please enter your email</p>
						<input type="email" name="user_email" placeholder="email..." required>
					</div>
					<div class="err-class">
						<?php  
							if(isset($_SESSION['find_account'])) {
								$error = $_SESSION['find_account'];

								if($error == "empty_fields") {
									echo "<span>please fill in all fields.</span>";
								} elseif ($error == "invalid_email") {
									echo "<span>email is invalid.</span>";
								} elseif ($error == "no_user") {
									echo "<span>there is no account with this email.</span>";
								} elseif ($error == "long_time") {
									echo "<span>please try again.</span>";
								} elseif ($error == "error_unknown") {
									echo "<span>something went wrong.</span>";
								} else {
									echo "";
                                }
                                
                                unset($_SESSION['find_account']);
							}
						?>
					</div>
					<div id="btn-gr" style="flex-direction: column;">
						<button style="width: 100%; padding: 10px; border-radius: 5px; margin-top: 15px;" type="submit" name="find_user">find account</button>
						<div style="align-self: flex-start; padding: 2px;">
							<p>new user?</p>
							<a href="signup.php">signup</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>