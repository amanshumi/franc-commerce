<?php
use YenePay\Models\IPN;
use YenePay\CheckoutHelper;

include_once 'php/db/connection.php';
include_once 'php/yenepay/CheckoutHelper.php';
include_once 'php/yenepay/Models/IPN.php';

session_start();

if (!isset($_SESSION['user_id'])) {
	header("Location: login.php");
} 

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Thank you</title>
	<link rel="stylesheet" type="text/css" href="styles/index.css">
	<script src="scripts/jquery-3.4.1.js"></script>
</head>

<body style="background: #f4f4f4;">
	<div id="all">
		<div id="app" class="head">
			<div class="nav">
				<div id="brand">
					<p>Frank</p>
				</div>
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="cart.php">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button style="font-family: material;" class="logout" type="submit" name="logout">exit_to_app</button>
					</form>
				</div>
				<div id="collapse">
					<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
				</div>
			</div>

			<div class="mobile-nav">
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="cart.php">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button class="llogout" type="submit" name="logout">logout</button>
					</form>
				</div>
			</div>

			<script>
				let switch_to = true;

				$("#collapse").hover(function() {
					$(this).css({
						opacity: "0.7",
						transition: "0.2s"
					});
				}, function() {
					$(this).css({
						opacity: "1",
						transition: "0.2s"
					});
				})

				$("#collapse").click(function() {
					this.switch_to = !this.switch_to;

					if (this.switch_to) {
						$("#shit").text("clear");
					} else {
						$("#shit").text("menu");
					}

					$(".mobile-nav").slideToggle("slow");
				})
			</script>
		</div>

		<div class="body" style="margin-top: 110px;">
			<div id="form-payment" style="background: #fff; border: none; box-shadow: 0px 0px 0px; font-family: poppins; height: 350px; padding: 40px; display: flex; justify-content: center; align-items: center;">
				<div class="text">
					<?php						
											
						$ipnModel = new IPN();
						$ipnModel->setUseSandbox(true); //set this to false on production
						
						$json_data = json_decode(file_get_contents('php://input'), true);
						
						if(isset($json_data["TotalAmount"]))
							$ipnModel->setTotalAmount($json_data["TotalAmount"]);
						if(isset($json_data["BuyerId"]))
							$ipnModel->setBuyerId($json_data["BuyerId"]);
						if(isset($json_data["MerchantOrderId"]))
							$ipnModel->setMerchantOrderId($json_data["MerchantOrderId"]);
						if(isset($json_data["MerchantId"]))
							$ipnModel->setMerchantId($json_data["MerchantId"]);
						if(isset($json_data["MerchantCode"]))
							$ipnModel->setMerchantCode($json_data["MerchantCode"]);
						if(isset($json_data["TransactionCode"]))
							$ipnModel->setTransactionCode($json_data["TransactionCode"]);
						if(isset($json_data["TransactionId"]))
							$ipnModel->setTransactionId($json_data["TransactionId"]);
						if(isset($json_data["Status"]))
							$ipnModel->setStatus($json_data["Status"]);
						if(isset($json_data["Currency"]))
							$ipnModel->setCurrency($json_data["Currency"]);
						if(isset($json_data["Signature"]))
							$ipnModel->setSignature($json_data["Signature"]);
						
						
						$helper = new CheckoutHelper();
						if ($helper->isIPNAuthentic($ipnModel))
							echo 'Success!';
						else
							echo 'Fail';

						var_dump($json_data);
					?>
				</div>
			</div>
		</div>
	</div>

</body>

</html>