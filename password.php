<?php  
	session_start();

	if (isset($_SESSION['user_id'])) {
		header("Location: home.php");
	} 

	if (!isset($_SESSION['password_privilege'])) {
		header("Location: login.php");
	}

	unset($_SESSION['reset_privilege']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reset Password</title>
	<link rel="stylesheet" type="text/css" href="styles/login.css">
	<script src="scripts/jquery-3.4.1.js"></script>
</head>
<body>
	<div id="main">
		<div class="nav">
			<div id="brand">
				<p>Frank</p>
			</div>
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="signup.php">signup</a>
			</div>
			<div id="collapse">
				<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
			</div>
		</div>
		<div class="mobile-nav">
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="signup.php">signup</a>
			</div>
		</div>

		<script>
			let switch_to = true;

			$("#collapse").hover(function() {
				$(this).css({opacity: "0.7", transition: "0.2s"});
			}, function() {
				$(this).css({opacity: "1", transition: "0.2s"});
			})

			$("#collapse").click(function() {
				this.switch_to = !this.switch_to;

				if(this.switch_to){
					$("#shit").text("clear");
				} else {
					$("#shit").text("menu");
				}

				$(".mobile-nav").slideToggle("slow");
			})
		</script>

		<div class="form-con">
			<div id="title" style="padding: 0px;">
				<h1>create a new password</h1>
			</div>

			<div id="form-gr" style="">
				<form action="php/changepass.php" method="POST">
					<div class="special" id="input" style="font-family: poppins; font-size: 14px;">
						<p style="font-size: 14px;">new password: </p>
						<input type="password" name="new_password" placeholder="new password..." required>
					</div>
					<div class="special" id="input" style="font-family: poppins; font-size: 14px;">
						<p style="font-size: 14px;">confirm password: </p>
						<input type="password" name="new_password_conf" placeholder="confirm password..." required>
					</div>
					<div class="err-class">
						<?php  
							if(isset($_SESSION['reset_pass'])) {
								$error = $_SESSION['reset_pass'];

								if($error == "empty_fields") {
									echo "<span>please fill in all fields.</span>";
								} elseif ($error == "invalid_pass") {
									echo "<span>password length should be more than 6 characters.</span>";
								} elseif ($error == "diff_pass") {
									echo "<span>different password.</span>";
								} elseif ($error == "error_pass") {
									echo "<span>something went wrong, try again.</span>";
								} else {
									echo "";
                                }
                                
                                unset($_SESSION['reset_pass']);
							}
						?>
					</div>
					<div id="btn-gr" style="flex-direction: column;">
						<button style="width: 100%; padding: 10px; border-radius: 5px; margin-top: 15px;" type="submit" name="change_pass">create</button>
						<div style="align-self: flex-start; padding: 2px;">
							<p>new user?</p>
							<a href="signup.php">signup</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>