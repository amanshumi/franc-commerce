<?php  
	include_once 'php/db/connection.php';
	session_start();

	if (!isset($_SESSION['user_id'])) {
		header("Location: login.php");
	} 

	if (!isset($_GET['name']) || $_GET['name'] == null || $_GET['name'] == "") {
		header("Location: home.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Frank Search</title>
	<link rel="stylesheet" type="text/css" href="styles/index.css">
	<script src="scripts/jquery-3.4.1.js"></script>
</head>
<body>	
	<div id="all">
		<div id="app" class="head">
			<div class="nav">
				<div id="brand">
					<p>Frank</p>
				</div>
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="cart.php">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button style="font-family: material;" class="logout" type="submit" name="logout">exit_to_app</button>
					</form>
				</div>
				<div id="collapse">
					<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
				</div>
			</div>

			<div class="mobile-nav">
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="cart.php">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button class="llogout" type="submit" name="logout">logout</button>
					</form>
				</div>
			</div>

			<script>
				let switch_to = true;

				$("#collapse").hover(function() {
					$(this).css({opacity: "0.7", transition: "0.2s"});
				}, function() {
					$(this).css({opacity: "1", transition: "0.2s"});
				})

				$("#collapse").click(function() {
					this.switch_to = !this.switch_to;

					if(this.switch_to){
						$("#shit").text("clear");
					} else {
						$("#shit").text("menu");
					}

					$(".mobile-nav").slideToggle("slow");
				})
			</script>

			<div class="show" id="search-box-search" style="">
				<form id="hate_mod" style="margin: auto;">
					<input type="text" name="name" placeholder="search here..." required>
					<button id="searcher" style="font-family: 'material'; font-size: 22px;">search</button>
				</form>
			</div>
		</div>

		<div class="body" style="border-top: 2px solid #f4f4f4;">			
				<?php  
					if (isset($_GET['name'])) {
						$product_name = filter_var(htmlspecialchars(mysqli_real_escape_string($conn, $_GET['name'])), FILTER_SANITIZE_STRING);

						$stmt = "select * from franc.product where product.product_name like '%$product_name%' and product.approved=1 and product.product_quantity>0 LIMIT 16;";
						$result = mysqli_query($conn, $stmt);

						echo "<p style='padding: 20px;'>".mysqli_num_rows($result)." results found for  \"$product_name\"</p>";;

						if(mysqli_num_rows($result) > 0) {
							echo "<div id='pro-all'>";
							while($product_row = mysqli_fetch_assoc($result)) {
								
								echo "<a href='product.php?id=".$product_row['product_id']."' id='product' style='text-decoration: none; color: #191919;'>";
								echo "<div class='pro-img'>
											<img src='images/".$product_row['product_name'].".jpeg'>
									  </div>";
								echo "<div id='spec'>
									    <div><p>".$product_row['product_name']."</p><p>price: ".$product_row['product_price']."ETB</p>";
								echo "</div><div><p style='padding: 10px 20px 10px 20px;
								border-top-left-radius: 50px;
								border-bottom-left-radius: 50px;
								background: #B824C2 !important;
								color: #fff;
								border: none;
								font-family: poppins;
								font-size: 13px;
								transition: opacity 0.3s;' id='getprov'>view</p></div>";
								echo "</div>";
								echo "</a>";
							}

							echo "</div>";
						} else {
							
						}
					}
				?>
		</div>

		<?php include 'includes/footer.php'; ?>

		<script>
			$(document).ready(function() {
				$("input").hover(function() {
					$("input").css({opacity: "0.8", transition: "0.2s"});
				}, function() {
					$("input").css({opacity: "1", transition: "0.2s"});
				})

				$("#searcher").hover(function() {
					$("#searcher").css({opacity: "0.8", transition: "0.2s"});
				}, function() {
					$("#searcher").css({opacity: "1", transition: "0.2s"});
				})
			})
		</script>
	</div>
	<script src="js/script.js"></script>

</body>
</html>