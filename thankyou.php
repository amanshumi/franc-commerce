<?php

use phpmailer\PHPMailer\PHPMailer;
use phpmailer\PHPMailer\Exception;

include 'php/phpmailer/PHPMailer.php';
include 'php/phpmailer/SMTP.php';
include 'php/phpmailer/Exception.php';
include_once 'php/db/connection.php';

session_start();

if (!isset($_SESSION['user_id'])) {
	header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Thank you</title>
	<link rel="stylesheet" type="text/css" href="styles/index.css">
	<script src="scripts/jquery-3.4.1.js"></script>
</head>

<body style="background: #f4f4f4;">
	<div id="all">
		<div id="app" class="head">
			<div class="nav">
				<div id="brand">
					<p>Frank</p>
				</div>
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="cart.php">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button style="font-family: material;" class="logout" type="submit" name="logout">exit_to_app</button>
					</form>
				</div>
				<div id="collapse">
					<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
				</div>
			</div>

			<div class="mobile-nav">
				<div id="navbar">
					<a href="home.php">home</a>
					<a href="cart.php">cart</a>
					<a href="profile.php">profile</a>
					<a href="#">about</a>
					<form action="php/logout.php" method="POST">
						<button class="llogout" type="submit" name="logout">logout</button>
					</form>
				</div>
			</div>

			<script>
				let switch_to = true;

				$("#collapse").hover(function() {
					$(this).css({
						opacity: "0.7",
						transition: "0.2s"
					});
				}, function() {
					$(this).css({
						opacity: "1",
						transition: "0.2s"
					});
				})

				$("#collapse").click(function() {
					this.switch_to = !this.switch_to;

					if (this.switch_to) {
						$("#shit").text("clear");
					} else {
						$("#shit").text("menu");
					}

					$(".mobile-nav").slideToggle("slow");
				})
			</script>
		</div>

		<div class="body" style="margin-top: 110px;">
			<div id="form-payment" style="background: #fff; border: none; box-shadow: 0px 0px 0px; font-family: poppins; height: 350px; padding: 40px; display: flex; justify-content: center; align-items: center;">
				<div class="text">
					<?php
					if (isset($_SESSION['payment_status'])) {
						$pay_status = $_SESSION['payment_status'];

						if ($pay_status == "completed") {
							echo "<h1 style='text-align: center;'>Thanks for purchasing</h1>
								<p>Next, check your email for the transaction code you received. </p>
								<div class='form-payment' style='width: 70%; margin:30px auto; display: flex; justify-content: space-around;'>
									<a href='https://mail.google.com/login' style='text-decoration: none; padding: 15px; font-size: 14px; background: purple; color: #fff; border-radius: 50px;'>proceed to email</a>
									<a href='home.php'style='text-decoration: none; padding: 15px; font-size: 14px; background: purple; color: #fff; border-radius: 50px;'>go to homepage</a>
								</div>";
						} else {
							echo "<h1 style='text-align: center; color: red'>Something went wrong</h1>
								<p>please refresh this page to get the order code through email. </p>
								
								<form id='form-pay-btn' style='display: flex; justify-content: center !important;' action='thankyou.php' method='POST'>
									<button id='button' name='email_retry_form' style='margin-auto !important;'>refresh</button>
								</form>";

							if (isset($_POST['email_retry_form'])) {
								$user_id = $_SESSION['user_id'];
								$first_name = $_SESSION['first_name'];
								$last_name = $_SESSION['last_name'];
								$user_email = $_SESSION['user_email'];
								$order_code = $_SESSION['order_code'];

								$mail = new PHPMailer();
								$mail->isSMTP();
								$mail->Host = "smtp.gmail.com";
								$mail->Port = 587;
								$mail->SMTPAuth = true;
								$mail->SMTPSecure = 'tls';
								$mail->Username = "amanuel8000@gmail.com";
								$mail->Password = "way100fournine";
								$mail->WordWrap = 40;
								$mail->setFrom("amanuel8000@gmail.com", "Frank Online Shopping");
								$mail->addAddress($user_email, $first_name . " " . $last_name);
								$mail->Subject = "Check Your Transaction Code";
								$mail->isHTML(true);
								$mail->Body = "<h2>Transaction Confirmation.</h2><br><p style='padding: 20px; color: #191919; text-align: center; text-decoration: none; border-radius: 5px; font-size: 24px;'>Your Order Code is: <strong>$order_code</strong></p>";

								$end_email_time = time() + 200;

								if ($mail->send()) {
									$_SESSION['payment_status'] = "completed";
									header("Location: thankyou.php");
								} elseif ($end_email_time && time() > $end_email_time || !$mail->send()) {
									$_SESSION['payment_status'] = "failed";
									header("Location: thankyou.php");
								}
							}
						}
					} else {
						header("Location: home.php");
					}
					?>
				</div>
			</div>
		</div>
	</div>

</body>

</html>