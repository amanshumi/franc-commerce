<?php
	include_once 'php/db/connection.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Registration Success</title>
	<link rel="stylesheet" type="text/css" href="styles/login.css">
</head>
<body>
	<div id="main">
	<div class="nav">
			<div id="brand">
				<p>frank</p>
			</div>
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="#" class="active">signup</a>
			</div>
			<div id="collapse">
				<span id="shit" style="font-family: 'material'; font-size: 30px; border: 1px solid #fff; border-radius: 3px;">menu</span>
			</div>
		</div>
		<div class="mobile-nav">
			<div id="navbar">
				<a href="login.php">login</a>
				<a href="signup.php">signup</a>
			</div>
		</div>

		<div class="form-con">
			<div id="title">
				<h1>Validation Success</h1>
			</div>

			<div id="form-gr" style="opacity: 0.8; margin: auto; border-left: 10px solid #B629D8; padding: 0px; font-size: 13px; width: 85%;">
				<?php 
					if (isset($_GET['user_email']) && isset($_GET['user_token'])) {
						$user_email = htmlspecialchars($_GET['user_email']);
						$user_token = htmlspecialchars($_GET['user_token']);

						$check_user = "select * from franc.users where user_email='$user_email' and confirmed=0 and token='$user_token';";
						$check_query = mysqli_query($conn, $check_user);

						if(mysqli_num_rows($check_query) > 0) {
							$validate_user = "update franc.users set confirmed=1, token='' where user_email='$user_email';";
							$validate_query = mysqli_query($conn, $validate_user);

							header("Location: login.php?login=login_to_continue");
						} else {
							header("Location: login.php");
						}
					} else {
						header("Location: signup.php");
					}
				?>
			</div>
		</div>
		<div id="copyright" style="padding: 5px; width: 80%; margin: 150px auto;">
			<p style="font-family: Josefin sans; text-align: center;">Frank &copy; 2020 All Rights Reserved.</p>
		</div>
	</div>
</body>
</html>